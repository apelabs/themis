/**
 * View Models used by Spring MVC REST controllers.
 */
package com.themis.web.rest.vm;

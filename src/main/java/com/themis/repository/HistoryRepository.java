package com.themis.repository;

import com.themis.domain.History;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the History entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HistoryRepository extends JpaRepository<History, Long> {

    @Query("select history from History history where history.user.login = ?#{principal.username}")
    List<History> findByUserIsCurrentUser();

}

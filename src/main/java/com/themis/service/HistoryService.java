package com.themis.service;

import com.themis.domain.History;
import com.themis.repository.HistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing History.
 */
@Service
@Transactional
public class HistoryService {

    private final Logger log = LoggerFactory.getLogger(HistoryService.class);

    private final HistoryRepository historyRepository;

    public HistoryService(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    /**
     * Save a history.
     *
     * @param history the entity to save
     * @return the persisted entity
     */
    public History save(History history) {
        log.debug("Request to save History : {}", history);
        return historyRepository.save(history);
    }

    /**
     *  Get all the histories.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<History> findAll(Pageable pageable) {
        log.debug("Request to get all Histories");
        return historyRepository.findAll(pageable);
    }

    /**
     *  Get one history by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public History findOne(Long id) {
        log.debug("Request to get History : {}", id);
        return historyRepository.findOne(id);
    }

    /**
     *  Delete the  history by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete History : {}", id);
        historyRepository.delete(id);
    }
}

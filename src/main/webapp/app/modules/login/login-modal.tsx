import * as React from 'react';
import { Card, CardTitle, CardActions, CardText, Button, TextField } from 'react-md';
import { reduxForm } from 'redux-form';
import * as TextInput from '../../shared/form-fields/TextInput'

export interface ILoginModalProps {
  showModal: boolean;
  loginError?: boolean;
  handleLogin: Function;
  handleClose: any;
}

export interface ILoginModalState {
  username: string;
  password: string;
}

class LoginModal extends React.Component<ILoginModalProps, ILoginModalState> {

  static defaultProps = {
    loginError: false
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      username: null,
      password: null
    };
  }

  handleSubmit = (values ) => {
    const { handleLogin } = this.props;
    const { username, password } = values;
    handleLogin(username, password, false);  // FIXME remember me value must be passed
  }

  handleUsernameChange = event => {
    this.setState({ username: event.target.value });
  }

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  }

  render() {
    const { loginError, handleClose } = this.props;

    return (
      <Card className="md-block-centered">
        <form onSubmit={this.handleSubmit}>
          <CardTitle
            title="Login"
            subtitle="Login using your credentials"
          />
          <CardText>
            <div className="row">
              <div className="col-md-12">
                { loginError ?
                  <div className="alert alert-danger">
                    Username or passsword not correct
                  </div>
                  : null
                }
              </div>
              <div className="col-md-12">
                  <TextInput.default
                    id="username"
                    name="username"
                    label="Username"
                    placeholder="username"
                    required errorText="Username cannot be empty!"
                  />
                  <TextInput.default
                    id="password" type="password"
                    name="password"
                    label="Password"
                    placeholder="password"
                    required errorText="Password cannot be empty!"
                  />
              </div>
            </div>
          </CardText>
          <CardActions>
            <Button type="submit" raised>Login</Button>
            <Button onClick={handleClose} flat>Cancel</Button>
          </CardActions>
        </form>
      </Card>
    );
  }
}

let createReduxForm = reduxForm({form: 'login'});

export default createReduxForm(LoginModal);

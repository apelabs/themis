import * as React from 'react';
import { connect } from 'react-redux';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';
import { MdBlock, MdSave } from 'react-icons/lib/md';

import { ICrudGetAction, ICrudPutAction } from '../../shared/model/redux-action.type';
import { getTicket, updateTicket, createTicket } from '../../actions/ticketActions';

export interface ITicketModalProps {
  getTicket: ICrudGetAction;
  updateTicket: ICrudPutAction;
  createTicket: ICrudPutAction;
  loading: boolean;
  updating: boolean;
  ticket: any;
  match: any;
  history: any;
}

export interface ITicketModalState {
  showModal: boolean;
  isNew: boolean;
}
export class TicketModal extends React.Component<ITicketModalProps, ITicketModalState> {

  constructor(props) {
    super(props);
    this.state = {
      showModal: true,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    !this.state.isNew && this.props.getTicket(this.props.match.params.id);
  }

  saveTicket = (event, errors, values) => {
    if (this.state.isNew) {
      this.props.createTicket(values);
    } else {
      this.props.updateTicket(values);
    }
    this.handleClose();
  }

  handleClose = () => {
    this.setState({
        showModal: false
    });
    this.props.history.push('/tickets');
  }

  render() {
    const isInvalid = false;
    const { ticket, loading, updating } = this.props;
    const { showModal, isNew } = this.state;
    return (
      <Modal
        isOpen={showModal} modalTransition={{ timeout: 20 }} backdropTransition={{ timeout: 10 }}
        toggle={this.handleClose} size="lg"
      >
      <ModalHeader toggle={this.handleClose}>Create or edit a User</ModalHeader>
      { loading ? <p>Loading...</p>
      : <AvForm model={isNew ? {} : ticket} onSubmit={this.saveTicket} >
          <ModalBody>

            <AvGroup>
              <Label for="name">Name</Label>
              <AvInput type="text" className="form-control" name="name" required />
              <AvFeedback>This field is required.</AvFeedback>
              <AvFeedback>This field cannot be longer than 50 characters.</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label for="description">Description</Label>
              <AvInput type="text" className="form-control" name="description" />
              <AvFeedback>This field cannot be longer than 50 characters.</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label for="label">Label</Label>
              <AvInput type="text" className="form-control" name="label" />
              <AvFeedback>This field cannot be longer than 50 characters.</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label for="ticketType">Ticket Type</Label>
              <AvInput type="text" className="form-control" name="ticketType" />
              <AvFeedback>This field cannot be longer than 50 characters.</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label for="status">Status</Label>
              <AvInput type="email" className="form-control" name="status" required/>
              <AvFeedback>This field is required.</AvFeedback>
              <AvFeedback>This field cannot be longer than 100 characters.</AvFeedback>
              <AvFeedback>This field is required to be at least 5 characters.</AvFeedback>
            </AvGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.handleClose}>
              <MdBlock/>&nbsp;
              Cancel
            </Button>
            <Button color="primary" type="submit" disabled={isInvalid || updating}>
              <MdSave/>&nbsp;
              Save
            </Button>
          </ModalFooter>
        </AvForm>
      }
    </Modal>
    );
  }
}

const mapStateToProps = storeState => ({
  ticket: storeState.ticket.ticket,
  loading: storeState.ticket.loading,
  updating: storeState.ticket.updating
});

const mapDispatchToProps = { getTicket, updateTicket, createTicket };

export default connect(mapStateToProps, mapDispatchToProps)(TicketModal);

import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { MdArrowBack } from 'react-icons/lib/md';

import { ICrudGetAction } from '../../shared/model/redux-action.type';
import { getTicket } from '../../actions/ticketActions';

export interface IUserManagementDetailProps {
  getTicket: ICrudGetAction;
  ticket: any;
  match: any;
}
export class TicketDetail extends React.Component<IUserManagementDetailProps, undefined> {

  componentDidMount() {
    this.props.getTicket(this.props.match.params.id);
  }

  render() {
    const { ticket } = this.props;
    return (
      <div>
        <div className="card">
          <div className="card-body">
          <h2>
            Ticket [<b>{ticket.id}</b>]
          </h2>
          <dl className="row-md jh-entity-details">
            <dt>Ticket</dt>
            <dd>
              <span>{ticket.id}</span>&nbsp;
            </dd>
            <dt>Name</dt>
            <dd>{ticket.name}</dd>
            <dt>Description</dt>
            <dd>{ticket.description}</dd>
            <dt>Status</dt>
            <dd>{ticket.status}</dd>
            <dt>Created By</dt>
            <dd>{ticket.createdBy ? ticket.createdBy.firstName : null}</dd>
            <dt>Created Date</dt>
            <dd>{ticket.createdDate}</dd>
            <dt>Last Modified By</dt>
            <dd>{ticket.lastModifiedUser ? ticket.lastModifiedUser.firstName : null}</dd>
            <dt>Last Updated Date</dt>
            <dd>{ticket.lastUpdatedDate}</dd>
          </dl>
          <Button
            tag={Link} to="/tickets" replace
            color="info"
          >
          <MdArrowBack/> <span className="d-none d-md-inline" >Back</span></Button>
          </div>          
        </div>
        <div className="card">
          <div className="card-header">
            <ul className="nav nav-tabs card-header-tabs">
              <li className="nav-item">
                <a className="nav-link active" href="#">Comments</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">History</a>
              </li>
            </ul>
          </div>
          <div className="card-body">
            <h4 className="card-title">Special title treatment</h4>
            <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" className="btn btn-primary">Go somewhere</a>
          </div>
        </div>
    </div>
    );
  }
}

const mapStateToProps = storeState => ({
  ticket: storeState.ticket.ticket
});

const mapDispatchToProps = { getTicket };

export default connect(mapStateToProps, mapDispatchToProps)(TicketDetail);

import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import { MdAdd, MdRemoveRedEye, MdEdit, MdDelete } from 'react-icons/lib/md';

import { ICrudGetAction } from '../../shared/model/redux-action.type';
import { getTickets } from '../../actions/ticketActions';

export interface ITicketsProps {
  getTickets: ICrudGetAction;
  tickets: any[];
  account: any;
  match: any;
}

export class Tickets extends React.Component<ITicketsProps, undefined> {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getTickets();
  }

  render() {
    const { tickets, account, match } = this.props;
    return (
      <div>
        <h2>
          Tickets
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity">
            <MdAdd /> Add Ticket
          </Link>
        </h2>
        <div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Status</th>
                <th>Assigned To</th>
                <th>Created Date</th>
                <th>Last Updated Date</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {
              tickets.map((ticket, i) => (
                <tr key={`user-${i}`}>
                  <td>
                    <Button
                      tag={Link} to={`${match.url}/${ticket.id}`}
                      color="link" size="sm"
                    >
                      {ticket.id}
                    </Button>
                  </td>
                  <td>{ticket.name}</td>
                  <td>{ticket.status}</td>
                  <td>{ticket.assigned_to_id}</td>
                  <td>{ticket.createdDate}</td>
                  <td>{ticket.lastUpdatedDate}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button
                        tag={Link} to={`${match.url}/${ticket.id}`}
                        color="info" size="sm"
                      >
                        <MdRemoveRedEye/> <span className="d-none d-md-inline" >view</span>
                      </Button>
                      <Button
                        tag={Link} to={`${match.url}/${ticket.id}/edit`}
                        color="primary" size="sm"
                      >
                        <MdEdit/> <span className="d-none d-md-inline">edit</span>
                      </Button>
                      <Button
                        tag={Link} to={`${match.url}/${ticket.id}/delete`}
                        color="danger" size="sm"
                      >
                        <MdDelete/> <span className="d-none d-md-inline">delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))
            }
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = storeState => ({
  tickets: storeState.ticket.tickets,
  account: storeState.authentication.account
});

const mapDispatchToProps = { getTickets };

export default connect(mapStateToProps, mapDispatchToProps)(Tickets);

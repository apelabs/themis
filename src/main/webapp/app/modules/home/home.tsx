import './home.scss';

import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { getSession } from '../../reducers/authentication';

export interface IHomeProp {
  account: any;
  getSession: Function;
}

export interface IHomeState {
  currentUser: any;
}

export class Home extends React.Component<IHomeProp, IHomeState> {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: props.account
    };
  }

  componentWillMount() {
    this.props.getSession();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      currentUser: nextProps.account
    });
  }

  render() {
    const { currentUser } = this.state;

    return (
      <div className="row">
        <div className="col-md-9">
          <h2>Welcome to Themis</h2>
          <p className="lead">This is your homepage</p>
          {
            (currentUser && currentUser.login) ? (
              <div>
                <div className="alert alert-success">
                  You are logged in as {currentUser.login}
                </div>
              </div>
            ) : (
              <div>
                <div className="alert alert-warning">
                  If you want to
                  <Link to="/login" className="alert-link"> sign in</Link>
                  , you can try the default accounts:
                  <br />- Administrator (login=&quot;admin&quot; and password=&quot;admin&quot;)
                  <br />- User (login=&quot;user&quot; and password=&quot;user&quot;).
                </div>

                <div className="alert alert-warning">
                  You do not have an account yet?&nbsp;
                  <a className="alert-link">Register a new account</a>
                </div>
              </div>
            )
          }
        </div>
        <div className="col-md-3 pad">
          <span className="hipster img-fluid rounded" />
        </div>
      </div>
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

const mapDispatchToProps = { getSession };

export default connect(mapStateToProps, mapDispatchToProps)(Home);

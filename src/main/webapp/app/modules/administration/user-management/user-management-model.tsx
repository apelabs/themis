import * as React from 'react';
import { connect } from 'react-redux';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';
import { MdBlock, MdSave } from 'react-icons/lib/md';

import { ICrudGetAction, ICrudPutAction } from '../../../shared/model/redux-action.type';
import { getUser, getRoles, updateUser, createUser } from '../../../reducers/user-management';

export interface IUserManagementModelProps {
  getUser: ICrudGetAction;
  getRoles: ICrudGetAction;
  updateUser: ICrudPutAction;
  createUser: ICrudPutAction;
  loading: boolean;
  updating: boolean;
  user: any;
  roles: any[];
  match: any;
  history: any;
}

export interface IUserManagementModelState {
  showModal: boolean;
  isNew: boolean;
}
export class UserManagementModel extends React.Component<IUserManagementModelProps, IUserManagementModelState> {

  constructor(props) {
    super(props);
    this.state = {
      showModal: true,
      isNew: !this.props.match.params || !this.props.match.params.login
    };
  }

  componentDidMount() {
    !this.state.isNew && this.props.getUser(this.props.match.params.login);
    this.props.getRoles();
  }

  saveUSer = (event, errors, values) => {
    if (this.state.isNew) {
      this.props.createUser(values);
    } else {
      this.props.updateUser(values);
    }
    this.handleClose();
  }

  handleClose = () => {
    this.setState({
        showModal: false
    });
    this.props.history.push('/admin/user-management');
  }

  render() {
    const isInvalid = false;
    const { user, loading, updating, roles } = this.props;
    const { showModal, isNew } = this.state;
    return (
      <Modal
        isOpen={showModal} modalTransition={{ timeout: 20 }} backdropTransition={{ timeout: 10 }}
        toggle={this.handleClose} size="lg"
      >
      <ModalHeader toggle={this.handleClose}>Create or edit a User</ModalHeader>
      { loading ? <p>Loading...</p>
      : <AvForm model={isNew ? {} : user} onSubmit={this.saveUSer} >
          <ModalBody>
            { user.id ?
              <AvGroup>
                <Label for="id">ID</Label>
                <AvInput type="text" className="form-control" name="id" required readOnly/>
              </AvGroup>
              : null
            }
            <AvGroup>
              <Label for="login">Login</Label>
              <AvInput type="text" className="form-control" name="login" required />
              <AvFeedback>This field is required.</AvFeedback>
              <AvFeedback>This field cannot be longer than 50 characters.</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label for="firstName">First Name</Label>
              <AvInput type="text" className="form-control" name="firstName" />
              <AvFeedback>This field cannot be longer than 50 characters.</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label for="lastName">Last Name</Label>
              <AvInput type="text" className="form-control" name="lastName" />
              <AvFeedback>This field cannot be longer than 50 characters.</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label for="email">E-mail</Label>
              <AvInput type="email" className="form-control" name="email" required/>
              <AvFeedback>This field is required.</AvFeedback>
              <AvFeedback>This field cannot be longer than 100 characters.</AvFeedback>
              <AvFeedback>This field is required to be at least 5 characters.</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label check inline>
                <AvInput type="checkbox" className="form-control" name="activated" />Activated
              </Label>
              <AvFeedback>This field is required.</AvFeedback>
              <AvFeedback>This field cannot be longer than 100 characters.</AvFeedback>
              <AvFeedback>This field is required to be at least 5 characters.</AvFeedback>
            </AvGroup>
            <AvGroup>
              <Label for="authorities">Language Key</Label>
              {/* TODO: fix issue in selecting the value */}
              <AvInput type="select" className="form-control" name="authorities" multiple>
                {roles.map(role => <option value={role} key={role}>{role}</option>)}
              </AvInput>
            </AvGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.handleClose}>
              <MdBlock/>&nbsp;
              Cancel
            </Button>
            <Button color="primary" type="submit" disabled={isInvalid || updating}>
              <MdSave/>&nbsp;
              Save
            </Button>
          </ModalFooter>
        </AvForm>
      }
    </Modal>
    );
  }
}

const mapStateToProps = storeState => ({
  user: storeState.userManagement.user,
  roles: storeState.userManagement.authorities,
  loading: storeState.userManagement.loading,
  updating: storeState.userManagement.updating
});

const mapDispatchToProps = { getUser, getRoles, updateUser, createUser };

export default connect(mapStateToProps, mapDispatchToProps)(UserManagementModel);

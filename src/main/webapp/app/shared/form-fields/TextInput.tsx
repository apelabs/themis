import * as React from 'react';
import TextField from 'react-md/lib/TextFields';
import { Field } from 'redux-form'

const renderTextField = ({ input, meta: { touched, error }, ...others }) => (
    <TextField {...input} {...others} error={touched && !!error} errorText={error} />
);

const TextInput = (props) => (
    <Field
        id={props.id}
        name={props.name}
        type="text"
        label={props.id}
        helpText={props.id}
        component={renderTextField}
        required={props.required}
        className="md-cell md-cell--12"
    />
  );
  
export default TextInput;
import './header.scss';

import * as React from 'react';
import { connect } from 'react-redux';
import {
  Navbar, Nav, NavItem, NavLink, NavbarToggler, NavbarBrand, Collapse,
  UncontrolledNavDropdown, DropdownToggle, DropdownMenu, DropdownItem
} from 'reactstrap';
import {
    MdHome, MdViewList, MdPermDataSetting, MdPerson, MdHealing,
    MdTune, MdFormatListNumbered, MdDataUsage, MdAssignment, MdPersonAdd, MdSettings, MdExitToApp,
    MdSchedule
  } from 'react-icons/lib/md';
import { NavLink as Link } from 'react-router-dom';
import LoadingBar from 'react-redux-loading-bar';

import appConfig from '../../../config/constants';

const BrandIcon = props => (
  <div {...props} className="brand-icon">
    <img
      src="static/images/logo-jhipster-react.svg"
      alt="Logo"
    />
  </div>
);

export interface IHeaderProps {
  isAuthenticated: any;
}

export class Header extends React.Component<IHeaderProps, { menuOpen: boolean }> {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false
    };
  }

  renderDevRibbon = () => (
    process.env.NODE_ENV === 'development' ?
      <div className="ribbon dev"><a href="">Development</a></div> :
      null
  )

  toggleMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
  }

  render() {
    const entityMenuItems = [
      <DropdownItem divider key="divider"/>,
      <DropdownItem tag={Link} key="tickets" to="/tickets"><MdDataUsage /> Tickets</DropdownItem>,
    ];
    const adminMenuItems = [
      <DropdownItem tag={Link} key="user-management" to="/admin/user-management"><MdPerson /> User Management</DropdownItem>,
      <DropdownItem tag={Link} key="metrics" to="/admin/metrics"><MdDataUsage /> Metrics</DropdownItem>,
      <DropdownItem tag={Link} key="health" to="/admin/health"><MdHealing /> Health</DropdownItem>,
      <DropdownItem tag={Link} key="configuration" to="/admin/configuration"><MdTune /> Configuration</DropdownItem>,
      /* TODO: audit menu */
      <DropdownItem tag={Link} key="logs" to="/admin/logs"><MdFormatListNumbered /> Logs</DropdownItem>,
      /* jhipster-needle-add-element-to-admin-menu - JHipster will add entities to the admin menu here */
      <DropdownItem tag={Link} key="docs" to="/admin/docs"><MdAssignment /> API Docs</DropdownItem>    ];
    const accountMenuItems = [];
    if (this.props.isAuthenticated) {
      accountMenuItems.push(
        <DropdownItem tag={Link} key="settings" to="/account/settings"><MdSettings /> Settings</DropdownItem>,
        <DropdownItem tag={Link} key="password" to="/account/password"><MdSchedule /> Password</DropdownItem>,
        <DropdownItem tag={Link} key="logout" to="/logout"><MdExitToApp /> Logout</DropdownItem>
      );
    } else {
      accountMenuItems.push(
        <DropdownItem tag={Link} key="login" to="/login"><MdPerson /> Login</DropdownItem>,
        <DropdownItem tag={Link} key="register" to="/register"><MdPersonAdd /> Register</DropdownItem>
      );
    }

    return (
      <div id="app-header">
        {this.renderDevRibbon()}
        <LoadingBar className="loading-bar"/>
        <Navbar dark expand="sm" fixed="top" className="jh-navbar">
          <NavbarToggler aria-label="Menu" onClick={this.toggleMenu} />
          <NavbarBrand tag={Link} to="/" className="brand-logo">
            <BrandIcon />
            <span className="brand-title">Themis</span>
            <span className="navbar-version">{appConfig.version}</span>
          </NavbarBrand>
          <Collapse isOpen={this.state.menuOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink tag={Link} to="/" className="d-flex align-items-center">
                  <MdHome />
                  <span>Home</span>
                </NavLink>
              </NavItem>
              {this.props.isAuthenticated ? [
                <UncontrolledNavDropdown key="entities">
                  <DropdownToggle nav caret className="d-flex align-items-center">
                    <MdViewList />
                    <span>Entities</span>
                  </DropdownToggle>
                  <DropdownMenu right>
                    {entityMenuItems}
                  </DropdownMenu>
                </UncontrolledNavDropdown>,
                <UncontrolledNavDropdown key="admin">
                  <DropdownToggle nav caret className="d-flex align-items-center">
                    <MdPermDataSetting />
                    <span>Administration</span>
                  </DropdownToggle>
                  <DropdownMenu right style={{ width: '120%' }}>
                    {adminMenuItems}
                  </DropdownMenu>
                </UncontrolledNavDropdown>
              ] : null}
              <UncontrolledNavDropdown>
                <DropdownToggle nav caret className="d-flex align-items-center">
                  <MdPerson />
                  <span>Account</span>
                </DropdownToggle>
                <DropdownMenu right>
                  {accountMenuItems}
                </DropdownMenu>
              </UncontrolledNavDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = storeState => ({
  isAuthenticated: storeState.authentication.isAuthenticated
});

export default connect(mapStateToProps)(Header);

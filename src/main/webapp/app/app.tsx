import './app.scss';

import * as React from 'react';
import { connect } from 'react-redux';
import { HashRouter  as Router, Route } from 'react-router-dom';
import { ModalContainer } from 'react-router-modal';

import { NavigationDrawer } from 'react-md';
import { Card } from 'react-md';

import { getSession, logout } from './reducers/authentication';
import Header from './shared/layout/header/header';
import Footer from './shared/layout/footer/footer';
import NavLink from './shared/layout/drawer/NavLink';
import AppRoutes from './routes';
export interface IAppProps {
  location: any;
  isAuthenticated?: boolean;
  getSession: Function;
  logout: Function;
  getSystemProperties: Function;
  routes: any;
}

const navItems = [{
  label: 'Home',
  to: '/',
  exact: true,
  icon: 'home',
}, {
  label: 'Login',
  to: '/login',
  icon: 'person_outline',
}, {
  label: 'Logout',
  to: '/logout',
  icon: 'person_outline',
}, {
  label: 'Settings',
  to: `/settings`,
  icon: 'settings',
}, {
  label: 'Tickets',
  to: `/tickets`,
  icon: 'apps',
}];

export class App extends React.Component<IAppProps, {}> {
  componentDidMount() {
    this.props.getSession();
  }

  handleLogout = () => {
    this.props.logout();
  }

  render() {
    const paddingTop = '60px';
    return (
      <Router>
        <Route render={({ location }) => (
        <NavigationDrawer
        drawerTitle="Themis"
        toolbarTitle="Themis"
        mobileDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
        desktopDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT_MINI}
        navItems={navItems.map(props => <NavLink {...props} key={props.to} />)}
        contentId="app-view-container"
        contentClassName="md-grid"
        >
          <div id="app-view-container">
            <Card>
              <AppRoutes/>
            </Card>
            <Footer/>
          </div>
          <ModalContainer />
        </NavigationDrawer>
        )} />
      </Router>
    );
  }
}

const mapStateToProps = storeState => ({
  isAuthenticated: storeState.authentication.isAuthenticated,
  embedded: storeState.layout.embedded
});

const mapDispatchToProps = { getSession, logout };

export default connect(mapStateToProps, mapDispatchToProps)(App);

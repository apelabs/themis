import axios from 'axios';

import { REQUEST, SUCCESS, FAILURE } from './action-type.util';
import { messages } from '../config/constants';
import { ICrudGetAction, ICrudPutAction, ICrudDeleteAction } from '../shared/model/redux-action.type';
import { ACTION_TYPES } from '../actions/ticketActions';

const initialState = {
  loading: false,
  errorMessage: null,
  tickets: [],
  ticket: {},
  updating: false,
  updateSuccess: false
};

// Reducer
export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TICKET):
    case REQUEST(ACTION_TYPES.FETCH_TICKETS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_TICKET):
    case REQUEST(ACTION_TYPES.UPDATE_TICKET):
    case REQUEST(ACTION_TYPES.DELETE_TICKET):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_TICKETS):
    case FAILURE(ACTION_TYPES.FETCH_TICKET):
    case FAILURE(ACTION_TYPES.CREATE_TICKET):
    case FAILURE(ACTION_TYPES.UPDATE_TICKET):
    case FAILURE(ACTION_TYPES.DELETE_TICKET):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_TICKETS):
      return {
        ...state,
        loading: false,
        tickets: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_TICKET):
      return {
        ...state,
        loading: false,
        ticket: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_TICKET):
    case SUCCESS(ACTION_TYPES.UPDATE_TICKET):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        ticket: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_TICKET):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        ticket: {}
      };
    default:
      return state;
  }
};
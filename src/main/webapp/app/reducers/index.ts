import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import layout from './layout';
import authentication from './authentication';
import administration from './administration';
import userManagement from './user-management';
import ticket from './ticket';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
  authentication,
  layout,
  administration,
  userManagement,
  ticket,
  form: formReducer,
  loadingBar
});
